import React, {Component} from "react";
import CurrenciesDisplay from "./CurrenciesDisplay";
import Currency from "./currency";

// The Application class is our main class.
// It is made of Menus, the page display area, and?
class Application extends Component {


    // In the constructor, we would like to fetch the currencies from the database, and pass them using DI to the currencies display
    // Therefore the application will have a set of currencies that it will keep at all times
    constructor(props) {
        super(props);

        // Names of the currencies; this should be fetched in the DB in a following step
        let currenciesNames = ["Etherum", "Bitcoin", "To", "The", "Moon"];

        let currencyList = currenciesNames.map((currencyName, index) => {
            return <Currency name={currencyName}></Currency>;
        });

        this.state = { currenciesToDisplay : currencyList};
    }

    // The render function is the code which will be injected into our index.html main page.
    render() {
        return (
            <div class="application">
                <h1 name="title">Crypto dashboard.</h1>

                <section>
                        <h1>Currencies</h1>
                        <div class="container">
                            <CurrenciesDisplay currenciesList = { this.state.currenciesToDisplay }></CurrenciesDisplay>
                        </div>
                </section>
            </div>
        );
    }

}

export default Application;