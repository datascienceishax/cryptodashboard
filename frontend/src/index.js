import _ from 'lodash';
import './styles.css'

import reactDom from 'react-dom';
import React from 'react';
import Application from './App';


// Wait until the whole DOM is loaded, otherwise document.getElementById may return NULL
window.onload = (event) => {

    // 
    const root = reactDom.createRoot(document.getElementById('root'));

    root.render(<Application></Application>);
}