import React, {Component} from 'react';

// Currencies Display will be our dashboard.
// Its goal will be to display the currencies that have been passed as parameter (prop).
class CurrenciesDisplay extends React.Component
{

    // Constructor 
    constructor(props) {
        super(props);


    }

    // This function will take the list of currencies that is in the state. It needs to be in the state
    // because it is dynamic, it will change upon user actions (sort currencies for instance)
    render() {
        return (
               this.props.currenciesList
        );
    }
}

export default CurrenciesDisplay;