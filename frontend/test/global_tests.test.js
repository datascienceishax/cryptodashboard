const {Builder, Browser, By, Key, until, Actions} = require('selenium-webdriver');
//const assert = require('assert');
var assert = require("chai").assert;

const webdriver = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
const firefox = require('selenium-webdriver/firefox');

BASE_URL = "http://127.0.0.1:8080";


describe('Hover over currencies test', () =>
{
    it ("Should change background color to red on hover", async function () {

             //To wait for browser to build and launch properly
            let driver = await new Builder().forBrowser("chrome").build();
    
            try {
                
                //To fetch our website (CryptoDashboard) url from the browser with our code.
                await driver.get(BASE_URL+"/index.html");
                
                //To send a search query by passing the value in searchString.
                let currencies = await driver.findElements(By.className("currency"));
            
                for (let currencyToCheckHover of currencies)
                {
                        let color = await currencyToCheckHover.getCssValue("background-color");
                           
                        assert.equal(color, "rgba(178, 213, 255, 1)");
                        
            
                        //Creating object of an Actions class
                        const actions = driver.actions();
                        //Performing the mouse hover action on the target element.
                        await actions.move({origin: currencyToCheckHover}).perform();
            
                        color = await currencyToCheckHover.getCssValue("background-color");
                        
                        assert.equal(color, "rgba(255, 0, 0, 1)");
                    
                }
        
            } finally {
                await driver.quit();
            }
    });
});
    


