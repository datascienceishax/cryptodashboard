const path = require('path');

module.exports = {
    entry: path.resolve(__dirname,'src/index.js'),
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js',
    },
    devServer: {
        host: "0.0.0.0",
        port: 8080,
        client: {
          overlay: {
            errors: true,
            warnings: false,
          },
        },
    },
    module: { 
    rules: [
                {
                    test: /\.css$/i,
                    use: [
                        // Creates 'style' nodes from JS strings
                        'style-loader', 
                        
                        // Translates CSS into CommonJS
                        'css-loader',

                    
                    ],
                },

                {
                    test: /\.s[ac]ss$/i,
                    use: [
                         // Creates 'style' nodes from JS strings
                         'style-loader', 
                        
                         // Translates CSS into CommonJS
                         'css-loader',
 
                         // Compiles Sass to CSS
                         'sass-loader',
                    ]

                },
                {
                    test: /\.m?js$/,
                    exclude: /(node_modules|bower_components)/,
                    use: {
                      loader: 'babel-loader',
                      options: {
                        presets: ['@babel/preset-env']
                      }
                    }
                  }
            ],
    },
};

