var assert = require("chai").assert;
let mongoose = require('mongoose');
var express = require('express');
let app = express();

const MONGODB_DATABASE_HOST = 'localhost';
const MONGODB_DATABASE_PORT = 27017;
const MONGODB_DATABASE_NAME = 'cryptodashboard_db1';


describe('Database connection test', () => {

    it('should not throw an error when connecting to the correct database', async function () {
        
        let res = 'unknown';
        
        // Connection to MongoDB database
        mongoose.createConnection('mongodb://' + MONGODB_DATABASE_HOST + ':' + MONGODB_DATABASE_PORT + '/' + MONGODB_DATABASE_NAME).asPromise().then(()=>{
            app.listen(MONGODB_DATABASE_PORT, ()=>{
                res = 'connection_success';
                console.log("Database connection is Ready and Server is Listening on Port: ", MONGODB_DATABASE_PORT);
               
            })
        })
        .catch((err)=>{
            res = err;
            console.log("A error has been occurred while connecting to database. Error: ", err);   
        }).finally(() =>
        {
            assert.notEqual(res, 'unknown');
            assert.equal(res, 'connection_success');
            connection.connection.close();
        });
  
       // let {CurrencySchema} = require('../schemas/Currency_schema');


 //       assert.notEqual(res, 'unknown');
      

    });

    it('should  throw an error when connecting to the wrong database host', async function () {
        
        let res = 'unknown';
        WRONG_MONGODB_DATABASE_HOST = '1.1.1.1';
        // Connection to MongoDB database
        mongoose.createConnection('mongodb://' + WRONG_MONGODB_DATABASE_HOST + ':' + MONGODB_DATABASE_PORT + '/' + MONGODB_DATABASE_NAME).asPromise().then(()=>{
            app.listen(MONGODB_DATABASE_PORT, ()=>{
                res = 'connection_success';
                console.log("Database connection is Ready and Server is Listening on Port: ", MONGODB_DATABASE_PORT);
               
            })
        })
        .catch((err)=>{
            res = 'connection_error';
            console.log("A error has been occurred while connecting to database. Error: ", err);   
        }).finally(() =>
        {
            assert.notEqual(res, 'unknown');
            assert.equal(res, 'connection_error');
            connection.connection.close();
        });
  
       // let {CurrencySchema} = require('../schemas/Currency_schema');


 //       assert.notEqual(res, 'unknown');
      

    });

})