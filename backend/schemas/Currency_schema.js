const { Double } = require("mongodb");
let mongoose = require("mongoose");

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const CurrencySchema = new Schema({
  id: ObjectId,
  currency_name: String,
  currency_description: String,
  currency_value: Number,
  currency_value_unit: String
}, {collection: "cryptocurrency"});

module.exports = {
    CurrencySchema : CurrencySchema
}